from django.test import TestCase, override_settings, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from .models import *

class UnitTest(TestCase):
    def test_views_success(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    def setUp(self):
        data_liked_book.objects.create(book_name="quiet",
        numberoflikes= 1)
    def test_models(self):
        obj = data_liked_book.objects.all().count()
        self.assertEqual(1, obj)
        obj1 = data_liked_book.objects.all()[0]
        self.assertEqual("Title: quiet", str(obj1))

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')
        # self.driver = webdriver.Firefox()
    def tearDown(self):
        self.driver.quit()
        super().tearDown()
    def test_page(self):
        #Adil open his Firefox and the website
        self.driver.get(self.live_server_url)
        
        #Adil see the items in the page
        time.sleep(2)
        table = self.driver.find_element_by_id('tableId')
        form = self.driver.find_element_by_id('prospects_form')
        text_field = self.driver.find_element_by_id('myInput')
        submit_button = self.driver.find_element_by_id('searchClick')

        #Adil the search box
        text_field.send_keys('quiet')
        submit_button.send_keys(Keys.ENTER)

        #Adil look at the content
        time.sleep(5)
        self.assertIn('Quiet', self.driver.page_source)
        self.assertIn('Susan', self.driver.page_source)
        self.assertIn('0', self.driver.page_source)
        
        #Adil click the like button
        like_button = submit_button = self.driver.find_element_by_class_name('like')
        like_button.send_keys(Keys.ENTER)
        time.sleep(2)
        self.assertIn('1', self.driver.page_source)        
        
        
