from django.urls import path
from story9app import views

urlpatterns = [
    path('', views.index),
    path('storage/', views.save_result)
]