from django.db import models

class data_liked_book(models.Model):
    book_name = models.CharField(max_length=20)
    numberoflikes = models.PositiveIntegerField(default=1, blank=True, null=True)
    def __str__(self):
        return "Title: " + self.book_name
